# compitohtml

Primo compito su Kobe Bryant di Lorenzo Tricarico

## Struttura

CI sono due siti navigabili:

1. Sito multi pagina composto da le seguenti pagine:
- Homepage.html
- Anagrafica.html
- Biografia.html
- Gallery.html
- Teams.html
- Successi.html

2. Sito singla pagina, comprendente tutte le pagine sopra indicate accessible dalla pagina:
- SinglePage.html

oltre le pagine c'e una folder "assets" contente:
- assetes/css ,folder che contiene i file css 
- assets/img/foto , folder che contiente le foto

##  DOM

### commenti
commmenti in css

/**/

commenti in HTML

< !-- -- >

## CSS

### esempi di CSS studiati ed utilizzati
- # per gli id
- . per le classi
- * tutti gli elementi
- , per mettere più selettori
- #id.class deve avere #id e .class
- #id .class l'elemento .class deve avere come padre #id
- #id > .class l'elemento .class deve avere come primo padre #id
- #id[attr="val"]
- #id:hover funzioni "speciali" sull'elemento (:hover :focus :first-child etc...)

### Librerie utiizzate
    <!-- documentazione icone: https://fontawesome.com/?utm_source=font_awesome_homepage&utm_medium=display&utm_campaign=fa5_released&utm_content=auto_modal -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <!-- documentazione bootstrap: https://getbootstrap.com/docs/4.4/getting-started/introduction/ -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- documentazione animazioni: https://animate.style/-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <link href="/assets/css/cssNewTricarico.css" rel="stylesheet">